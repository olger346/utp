import RPi.GPIO as GPIO
from time import sleep
from subprocess import call
from w1thermsensor import W1ThermSensor
import requests
from datetime import datetime

sensor = W1ThermSensor()
sensor_kj = 21
sensor_mq = 17
pin_green = 18
TOKEN = "A1E-ExILPUqqZcRxOqUq7WVSwM5CoYZFL9"  # Put your TOKEN here
DEVICE_LABEL = "RPi3-client1000"  # Put your device label here 
VARIABLE_LABEL_1 = "temperature"  # Put your first variable label here
VARIABLE_LABEL_2 = "IR"  # Put your second variable label here
VARIABLE_LABEL_3 = "Smoke"  # Put your second variable label here

def build_payload(temperatura, kj, mq):

    value_1 = temperatura
    value_2 = kj
    value_3 = mq
    tiempo = datetime.now().time() 
    tiempo = str(tiempo)
    # Creates a random gps coordinates
    #lat = '9.0224'
    #lng = '-79.5317'
    payload = {'temperatura': value_1,
               'IR': value_2,
               'Smoke_detector': value_3}
    return payload

def post_request(payload):
    # Creates the headers for the HTTP requests
    url = "http://things.ubidots.com"
    url = "{}/api/v1.6/devices/{}".format(url, DEVICE_LABEL)
    headers = {"X-Auth-Token": TOKEN, "Content-Type": "application/json"}

    # Makes the HTTP requests
    status = 400
    attempts = 0
    while status >= 400 and attempts <= 3:
        req = requests.post(url=url, headers=headers, json=payload)
        status = req.status_code
        attempts += 1
        sleep(0.5)

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
GPIO.setup(26, GPIO.OUT, initial=GPIO.HIGH)
GPIO.setup(20, GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(pin_green, GPIO.OUT, initial=GPIO.LOW) 	##LED
GPIO.setup(sensor_mq, GPIO.IN)				##MQ-135 Calidad del aire	
GPIO.setup(sensor_kj, GPIO.IN)				##KJ-026 IR
i =0

while True:
	if (i ==0):
		GPIO.output(26, GPIO.HIGH)
	elif(i ==1):
		GPIO.output(26, GPIO.LOW)
		GPIO.output(20, GPIO.HIGH) 
	temperature = sensor.get_temperature()
	print("La temperatura es de %s celsius" % temperature)
	print("sensor IR ")
	print(GPIO.input(sensor_kj))
	control1 = GPIO.input(sensor_kj)
	print("sensor mq")
	print(GPIO.input(sensor_mq))
	control2 = GPIO.input(sensor_mq)
	sleep(3)
	if((control1==1  or control2 ==1) and temperature > 30):
		call(["sh", "/home/pi/comando.sh"])
		sleep(3)
		GPIO.output(26, GPIO.LOW)
		i = 1
	elif((control1==0 or control2==0) and temperature > 29):
		#call(["sh", "/home/pi/comando.sh"])
		sleep(3)
		GPIO.output(20, GPIO.LOW)
		if i == 1:
			GPIO.output(26, GPIO.LOW)
			GPIO.output(20, GPIO.HIGH)
			sleep(1)
			call(["sh", "/home/pi/comando2.sh"])
			i = 0
#			cal(["sh", "/home/pi/comando2.sh"])
	#elif(control1 or control2 ==0 and temperature <30):
	#	GPIO.output(pin_green, GPIO.HIGH)
	payload = build_payload(temperature, control1, control2)
	post_request(payload)
	print("done")
