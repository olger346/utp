import RPi.GPIO as GPIO
from time import sleep
from subprocess import call
from w1thermsensor import W1ThermSensor
import requests
from datetime import datetime

sensor = W1ThermSensor()
sensor_kj = 21
sensor_mq = 17
pin_red = 18
TOKEN = "BBFF-AaPuISD9YKe5tKLURn4DHNEs5d6Qum"  # Token de la aplicacion
DEVICE_LABEL = "RPi3-client-100"  # Poner el nombre del cliente 
VARIABLE_LABEL_1 = "temperature"  # Nombre de la primera variable
VARIABLE_LABEL_2 = "IR"  		  # Nombre de la segunda variable
VARIABLE_LABEL_3 = "Smoke"  	  # Nombre de la tercera variable

def build_payload(temperatura, kj, mq):

    value_1 = temperatura
    value_2 = kj
    value_3 = mq
    tiempo = datetime.now().time() 
    tiempo = str(tiempo)
 
    payload = {'temperatura': value_1,
               'IR': value_2,
               'Smoke_detector': value_3}
    return payload

def post_request(payload):
    # Creates the headers for the HTTP requests
    url = "http://things.ubidots.com"
    url = "{}/api/v1.6/devices/{}".format(url, DEVICE_LABEL)
    headers = {"X-Auth-Token": TOKEN, "Content-Type": "application/json"}

    # Makes the HTTP requests
    status = 400
    attempts = 0
    while status >= 400 and attempts <= 3:
        req = requests.post(url=url, headers=headers, json=payload)
        status = req.status_code
        attempts += 1
        sleep(1)

GPIO.setmode(GPIO.BCM)
GPIO.setup(pin_red, GPIO.OUT, initial=GPIO.LOW) 	##LED
GPIO.setup(sensor_mq, GPIO.IN)				##MQ-135 Calidad del aire	
GPIO.setup(sensor_kj, GPIO.IN)				##KJ-026 IR
while True:
	temperature = sensor.get_temperature()
	control1 = GPIO.input(sensor_kj)
	control2 = GPIO.input(sensor_mq)
	sleep(1)
	if(control1 or control2 ==1 and temperature > 33):
		GPIO.output(pin_red, GPIO.HIGH)
		call(["sh", "/home/pi/comando.sh"])
		sleep(30)
	elif(control1 or control2 ==0 and temperature > 30):
		sleep(4)
	elif(control1 and control2 ==0 and temperature <30):
		sleep(4)
	else:
		sleep(4)
	payload = build_payload(temperature, control1, control2)
	post_request(payload)