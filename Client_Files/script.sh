#!/bin/bash
sudo apt-get update -y
sudo apt-get install -y python3-pip python-pip git python3-dev python-dev
cd $HOME/Desktop && git clone https://gitlab.com/olger346/utp.git
sudo pip3 install requests websockets==4.0.1 pyyaml w1thermsensor RPi.GPIO hbmqtt==0.9.1
cd $HOME/Desktop/utp/Client_Files/ && mv read2.py $HOME/Desktop/
cp $HOME/Desktop/utp/Client_files/comando.sh /home/pi/
cp $HOME/Desktop/utp/Client_files/comando2.sh /home/pi/
cd $HOME/Desktop/utp/Client_Files/ && mv handler.py $HOME/Desktop/
sudo rm /usr/local/lib/python3.5/dist-packages/hbmqtt/mqtt/protocol/handler.py
sudo cp $HOME/Desktop/handler.py /usr/local/lib/python3.5/dist-packages/hbmqtt/mqtt/protocol/handler.py
