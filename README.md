# This is a modification of MQTT protocol for make an evacuation System
The main code is  [HBMQTT](https://github.com/beerfactory/hbmqtt)
And all right by their team.

HBMQTT v0.9.1
I just added some functionalities on broker.py and Session.py for Broker and Clients
The MQTT clients collect data from MQ-135, KJ-026, and DS18B20 sensors.
When it detects the prescence of fire, they trigger the publish funcionality.
When the broker gets this message; inmediately it discard routes to exit from
their subscribers.

Once the publish is triggered, the clients recieve a MQTT publish, and they turn on the green lights
which is an visual indicator of an available exit. Instead the red lights is a visual indicator of wrong exit.

The implementation is made it on Raspberry Pi3 and Raspberry Zero W. *Clients*
The main broker is running on a better Device, but it should work fine on RPi3.

## Broker Files
in broker files we find files which should be replaced on /usr/local/lib/python3/dist-packages/hbmqtt
the root of the program. In the root should replace the following files
1. broker.py
2. session.py

And in the /usr/local/lib/python3/dist-packages/hbmqtt/mqtt/protocol
replace 
1. handler.py

## Copy and paste the *Clients* files and run the read2.py 
in the RPi clients you should replace the following files
1. handler.py

And place the following files on RPi
1. read2.py ~ on Desktop
2. command.py ~ on your $HOME path
3. command2.py ~ on your $HOME path

Thats all. Read the wiki for more info [Wiki](https://gitlab.com/olger346/utp/wikis/Gu%C3%ADa-de-Instalaci%C3%B3n)